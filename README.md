# Midterm report
- Juan Sebastian Torres Daza - js.torres1@uniandes.edu.co - @juansetorres
- Juan Diego Trujillo Montoya - jd.trujillom@uniandes.edu.co - @juandiegotm
- Daniel Iván Romero Méndez - di.romero@uniandes.edu.co - https://gitlab.com/diromero1
 
# App Description

Brave is a browser that helps with cookies and add problems for the user. Also it helps out with vpn and firewall tracking in some sites. It has a free version or premium that also has a family package. Their revenue is due subs to premium, partner apps and donations people made to the company in order to improve the app. The users are around 20Million Monthly active. The app is interesting because it helps with much of the interrupting time in some pages due to advertisements, also has a nice interface that looks good in the phone colors are quite good and the orange lion seems to me a good logo.

![](RackMultipart20210322-4-156z08k_html_ce450db89460b8f7.png) ![](RackMultipart20210322-4-156z08k_html_d91b15bfed1e909e.pn# g)

# Repo Description

The Company has many repos and one unique for IOS using swift, in the case of android they are in the core repo. Actually they use a lot of languages in that repository.

![](RackMultipart20210322-4-156z08k_html_b50c72deae0af75e.png)

Fortunately Android got their own module to work with. The main code of the module is Java. It has 20309 commits at moment. With 5 million code lines approx.

![](RackMultipart20210322-4-156z08k_html_4293288d41959697.png)

# Architecture

Brave browser is based in chromium project. It uses the same architecture

**Deployment view:**

![](RackMultipart20210322-4-156z08k_html_be950c213e2a06e1.png)

**Behavioural view:**

![](RackMultipart20210322-4-156z08k_html_9a7d9fc06e5434c7.png)

Front end architecture follow MVVM architectural patterns which consists on three tier communication one direction oriented. Also, there is a service broker (channel) in the Model tier which encapsulates the IPC procedures to the back end service. The latter is built with a balancer which shows the application services that remotely connect with the Chromium facade, which is in charge of following the process of rendering and resource dispatcher that are kind of black box to as, because this tool is from another party.

**Builder (flags)**

![](RackMultipart20210322-4-156z08k_html_8477ccf2d807643a.png)

![](RackMultipart20210322-4-156z08k_html_bb952ac8e5d1c3b3.png) Basado en eventos.

Observer Design pattern

![](RackMultipart20210322-4-156z08k_html_dda0644299db79c6.png)

The pattern locates the publishers of the crypto money and it refresh the values on real time.

**Builder**

![](RackMultipart20210322-4-156z08k_html_e2c3017102d30fe8.png)

![](RackMultipart20210322-4-156z08k_html_3a6791b466ed0581.png)

The brave organization uses flags as a builder for many instances in order to test or use diferent versions of certain methods and codes.

**Factory**

![](RackMultipart20210322-4-156z08k_html_9856fe8d7f77d08d.png)

This methods creates a new object of the instance in certain context like a factory of products.

**Chromium Observer Application Service**

![](RackMultipart20210322-4-156z08k_html_70102859c44c899d.png)

Used for communication between routines as to maintain events flowing in asynchronous order as shown below:

![](RackMultipart20210322-4-156z08k_html_82ffa1f117f1b8a2.png)

**Three design patterns.**

Based on this classes, there are three patters:

![](RackMultipart20210322-4-156z08k_html_a68fe5a72e8f413.png)

1. Proxy

![](RackMultipart20210322-4-156z08k_html_1c882d810f719a21.png)

1. Bridge

![](RackMultipart20210322-4-156z08k_html_4db20eb222387e74.png)

1. Observer

![](RackMultipart20210322-4-156z08k_html_b55b39eb7e70c436.png)

**Singleton**

![](RackMultipart20210322-4-156z08k_html_2ec11c8ded066a58.png)

# Quality Attributes

**Portability**: The Application run son different devices. It runs on the following operating systems: Linux/Android, macOS, iOS and also windows. This quality attribute is needed in order to reach a big quantity of users.

**Accessibility**: The Application is user friendly, which allows anyone to search easily. This is accomplished by a minimalist design with the navigation bar on top of one toolbar with big buttons to manage the history (going back and forward), a searching bar and more options, which resembles the chrome app as shown below.

![](RackMultipart20210322-4-156z08k_html_51f6ab1cb3164908.png)

It also has user friendly designs on mobile devices.

**Latency**: The Application should be capable of searching among the web pages and domain names on a quick manner. Users need to find their answers easy and fast. Google search still a competitor which answers queries fast. That is why they use the Chromium API, to use the functionality already built by Google which does the work efficiently.

Maintainability: The Application is a project built among more than a hundred contributors and it&#39;s last merged pull request was about 9 minutes ago from the time this report was written as it is showed below:

![](RackMultipart20210322-4-156z08k_html_79a7f9e44df9bede.png)

Also, the number of contributors is shown below:

![](RackMultipart20210322-4-156z08k_html_c765bb3ff5d3b3b1.png)

They are using clear design patterns to maintain the code and keep it from getting a total mess. They also must have documentation for every decision taken in the construction of the app, but they keep it just for the contributors.

Security: The Application must keep the DNS servers protected from a poisoning attack. This is accomplished by using the Chromium API which already takes measurements to sanitize the inputs that reach the DNS server.

# Libraries and dependencies

The main dependency of the project is _Chromium_, which is an API that gives one of the core services of the application which is to fetch web pages. So, big part of the application&#39;s functionality is built upon the services offered by this API. Also, there is a dependency with the _adblock-rust_ project which they highlight, and it is used to block ads and pop ups.

There are also libraries that the application fetches and syncs code from which are used to give web structure to the application such as:

- Babel
- Webtorrent
- Webpack
- Redux
- Storybook

Just to mention the main ones, but the list goes for more than a thousand dependencies in libraries. Also, there are some dependencies used for Analytics such as:

- Scikit-learn
- Matplotlib
- Jupyterlab
- Pandas
